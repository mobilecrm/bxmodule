<?php

namespace MobileCrm\Services;

class Router
{
    const RESPONSE_BAD_REQUEST = 400;
    const RESPONSE_OK = 200;
    const RESPONSE_SERVER_ERROR = 500;
    const RESPONSE_NOT_FOUND = 404;
    const RESPONSE_UNAUTHORIZED = 401;

    public static function response($data, $status){
        echo json_encode([
            'result'=>$data,
            'status'=>$status
        ]);
        http_response_code($status);
        die();
    }

    public static function checkRequired($request, $required){
        foreach($required as $field){
            if($request[$field])
                continue;
            else
                self::response([
                    'error'=>"Missed `{$field}` param"
                ], self::RESPONSE_BAD_REQUEST);
        }
    }

    public static function checkAuth($request){
        if($request['api_token']){
            $userId = Auth::decodeToken($request['api_token']);
            if($userId)
                return $userId;
        }
        self::response(false, self::RESPONSE_UNAUTHORIZED);
    }
}