<?php


namespace MobileCrm\Services;


class Cipher
{
    public static function encode($unencoded,$key){
        $string=base64_encode($unencoded);

        $arr=[];
        $x=0;
        $newString = '';

        while ($x++< strlen($string)) {
            $arr[$x-1] = md5(md5($key.$string[$x-1]).$key);
            $newString .= $arr[$x-1][3].$arr[$x-1][6].$arr[$x-1][1].$arr[$x-1][2];
        }

        return $newString;
    }

    public static function decode($encoded, $key){
        $base64sybmols="qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM=";
        $x=0;
        while ($x++<= strlen($base64sybmols)) {//Цикл
            $tmp = md5(md5($key.$base64sybmols[$x-1]).$key);
            $encoded = str_replace($tmp[3].$tmp[6].$tmp[1].$tmp[2], $base64sybmols[$x-1], $encoded);
        }
        return base64_decode($encoded);
    }
}