<?php


namespace MobileCrm\Services;


use Bitrix\Main\UserTable;

class Auth
{
    public static function signIn($request){
        Router::checkRequired($request, ['login','password']);
        $user = new \CUser();
        $res = $user->Login($request['login'], $request['password']);
        if($res['TYPE']=="ERROR")
            Router::response($res['MESSAGE'], Router::RESPONSE_UNAUTHORIZED);

        $userId = $user->GetID();
        return Cipher::encode($userId, Config::SALT);
    }

    public static function decodeToken($token){
        $userId = Cipher::decode($token, Config::SALT);
        if(UserTable::getById($userId)->fetch())
            return $userId;
        return false;
    }
}