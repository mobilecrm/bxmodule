<?php

namespace MobileCrm\Entities;

use Bitrix\Crm\ContactTable;

class Contact
{
    public static function list($request){
        $request['order']=['ID'=>'DESC'];
        $result = ContactTable::getList($request)->fetchAll();
        if($result)
            return $result;
        else return null;
    }

    public static function get($request)
    {
        $filter['filter']['ID'] = $request['id'];
        if ($filter['select']) {
            $filter['filter']['select'] = $filter['select'];
        }
        $result = ContactTable::getList($filter)->fetch();
        if ($result)
            return $result;
        else return null;
    }
}