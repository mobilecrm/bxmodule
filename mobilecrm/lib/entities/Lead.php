<?php

namespace MobileCrm\Entities;

use Bitrix\Crm\LeadTable;

class Lead
{
    public static function list($request){
        $request['order']=['ID'=>'DESC'];
        $result = LeadTable::getList($request)->fetchAll();
        if($result)
            return $result;
        else return null;
    }

    public static function get($request)
    {
        $filter['filter']['ID'] = $request['id'];
        if ($filter['select']) {
            $filter['filter']['select'] = $filter['select'];
        }
        $result = LeadTable::getList($filter)->fetch();
        if ($result)
            return $result;
        else return null;
    }
}