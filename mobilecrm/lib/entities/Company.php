<?php

namespace MobileCrm\Entities;

use Bitrix\Crm\CompanyTable;

class Company
{
    public static function list($request){
        $request['order']=['ID'=>'DESC'];
        $result = CompanyTable::getList($request)->fetchAll();
        if($result)
            return $result;
        else return null;
    }

    public static function get($request){
        $filter['filter']['ID'] = $request['id'];
        if($filter['select']){
            $filter['filter']['select'] = $filter['select'];
        }
        $result = CompanyTable::getList($filter)->fetch();
        if($result)
            return $result;
        else return null;
    }
}