<?php


namespace MobileCrm\General;


use Bitrix\Crm\CompanyTable;
use Bitrix\Crm\ContactTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\LeadTable;


class CrmPermissions
{
    const PERM_TYPE_ALL = "X";
    const PERM_TYPE_SELF = "A";
    const PERM_TYPE_DEP = "D";
    const PERM_TYPE_OPENED = "O";
    const PERM_TYPE_DEP_AND_DOWN = "F";

    const ACTION_READ = "READ";
    const ACTION_ADD = "ADD";
    const ACTION_WRITE = "WRITE";
    const ACTION_DELETE = "DELETE";

    const DEAL = "DEAL";
    const CONTACT = "CONTACT";
    const COMPANY = "COMPANY";
    const LEAD = "LEAD";

    public static function getUserPermissions($id, $entity){
        $perms = new \CCrmPerms($id);
        $res = $perms->GetUserPerms()[strtoupper($entity)];
        foreach($res as $action=>$perm){
            if(!$perm['-']){
                $result[$action] = false;
                continue;
            }
            $result[$action] = $perm['-'];
        }
        return $result;
    }

    public static function getFilter($id, $permType){
        switch ($permType){
            case self::PERM_TYPE_ALL:
                return [];
            case self::PERM_TYPE_SELF:
                return ['ASSIGNED_BY_ID'=>$id];
            case self::PERM_TYPE_DEP:
                $deps = \CIntranetUtils::GetUserDepartments($id);
                $depUsers = \CIntranetUtils::getDepartmentEmployees($deps);
                while($ob = $depUsers->fetch()){
                    $result[] = $ob['ID'];
                }
                return ['ASSIGNED_BY_ID'=>$result];
            case self:: PERM_TYPE_DEP_AND_DOWN:
                $deps = \CIntranetUtils::GetUserDepartments($id);
                $subDeps = [];
                foreach ($deps as $dep) {
                    $subDeps = array_merge($subDeps, \CIntranetUtils::GetDeparmentsTree($dep, true));
                }
                $allDeps = array_merge($deps, $subDeps);
                $depUsers = \CIntranetUtils::getDepartmentEmployees($allDeps);
                while($ob = $depUsers->fetch()){
                    $result[] = $ob['ID'];
                }
                return ['ASSIGNED_BY_ID'=>$result];
            case self::PERM_TYPE_OPENED:
                return ['OPENED'=>'Y'];
        }
    }

    public static function checkAccessForEntity($crmId, $crmType, $user, $action){
        $permsForAction = self::getUserPermissions($user, $crmType)[$action];
        $filter = self::getFilter($user, $permsForAction);
        $filter['ID'] = $crmId;
        switch ($crmType) {
            case self::LEAD:
                $res = LeadTable::getList([
                    'filter'=>$filter
                ]);
                break;
            case self::COMPANY:
                $res = CompanyTable::getList([
                    'filter'=>$filter
                ]);
                break;
            case self::CONTACT:
                $res = ContactTable::getList([
                    'filter'=>$filter
                ]);
                break;
            case self::DEAL:
                $res = DealTable::getList([
                    'filter'=>$filter
                ]);
                break;
            default: return false;
        }
        return !!$res->fetch();
    }
}