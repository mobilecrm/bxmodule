<?php

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

try {
    Loader::registerAutoloadClasses(
        "mobilecrm",
        [
            '\MobileCrm\Services\Auth'=>'/lib/services/Auth.php',

            '\MobileCrm\General\CrmPermissions'=>'/lib/general/CrmPermissions.php',

            '\MobileCrm\Services\Cipher'=>'/lib/_services/Cipher.php',
            '\MobileCrm\Services\Config'=>'/lib/_services/Config.php',
            '\MobileCrm\Services\Router'=>'/lib/_services/Router.php',

            '\MobileCrm\Entities\Lead'=>'/lib/entities/Lead.php',
            '\MobileCrm\Entities\Contact'=>'/lib/entities/Contact.php',
            '\MobileCrm\Entities\Company'=>'/lib/entities/Company.php',
            '\MobileCrm\Entities\Deal'=>'/lib/entities/Deal.php',
        ]
    );
} catch (LoaderException $e) {
    Debug::writeToFile($e->getMessage(), "", "/mobilecrm.log");
}
