<?php

use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\EventManager;
use Bitrix\Main\UrlRewriter;

class mobilecrm extends CModule
{
    var $MODULE_ID = "mobilecrm";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;

    function mobilecrm()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Мобильная CRM";
        $this->MODULE_DESCRIPTION = "Удобное веб-приложение для работы с CRM";
        $this->PARTNER_NAME = "Артем Липовой";
        $this->PARTNER_URI = "";
    }

    function DoInstall()
    {
        $this->installRouter();
        $this->InstallEvents();
        $this->InstallFiles();
        $this->InstallFields();
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function DoUninstall()
    {
        $this->unInstallRouter();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallFields();
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function installRouter(){
        try {
            UrlRewriter::add(1, [
                "CONDITION" => '#^/mobilecrm/rest/(?<path>)#',
                "RULE" => 'path=$1',
                "ID" => $this->MODULE_ID,
                "PATH" => '/local/modules/mobilecrm/services/router.php',
            ]);
        } catch (ArgumentNullException $e) {
            Debug::writeToFile($e->getMessage(), "", "/mobilecrm.log");
        }
    }

    function unInstallRouter(){
        try {
            UrlRewriter::delete(1, [
                "ID" => $this->MODULE_ID,
            ]);
        } catch (ArgumentNullException $e) {
            Debug::writeToFile($e->getMessage(), "", "/mobilecrm.log");
        }
    }

    function InstallEvents(){
        return true;
    }

    function UnInstallEvents(){
        return true;
    }

    function InstallFields(){
        return true;
    }

    function UnInstallFields(){
        return true;
    }

    function InstallFiles(){
        return true;
    }

    function UnInstallFiles(){
        return true;
    }

}
