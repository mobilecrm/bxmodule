<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use MobileCrm\Services\Router;

try {
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");

    Loader::includeModule('crm');
    Loader::includeModule('mobilecrm');

    $path = $_REQUEST['path'];
    $query = json_decode(file_get_contents("php://input"), true);
    if(!$path)
        Router::response(false, Router::RESPONSE_BAD_REQUEST);
    $pathEls = explode('/',$path);
    foreach($pathEls as $key=>$item){
        if($key==(count($pathEls)-1)){
            $methodName = $item;
        }else {
            $classPath[] = ucfirst($item);
        }
    }
    $classPathStr = implode("\\", $classPath);
    $path = "MobileCrm\\$classPathStr::$methodName";
    if(is_callable($path)) {
        $result = call_user_func($path, $query);
        if($result===null)
            Router::response(null, Router::RESPONSE_NOT_FOUND);
        Router::response($result, Router::RESPONSE_OK);
    }
    Router::response(false, Router::RESPONSE_BAD_REQUEST);

} catch (LoaderException $e) {
    Debug::writeToFile($e->getMessage(), "", "/mobilecrm.log");
    Router::response(false, Router::RESPONSE_SERVER_ERROR);
}
